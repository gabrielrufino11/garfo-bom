<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Login/login.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Login/login.php');
		});
		
		$('#AreadoProfessor').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$("#myModal").on("hidden.bs.modal", function () {
	
			$('#loader').load('view/Login/login.php');
		});
		
		$('#Login').click(function(e) {
			
			//loader
			$('#myModal').modal('hide');			
    		
		});
		
		$('#Salvar').click(function(e) {
			e.preventDefault();
			
			//1 instansciar e recuperar valores dos inputs
			
			var id_Professor =$('#id_Professor').val();
			var nome_Professor =$('#nome_Professor').val();
			var email_Professor = $('#email_Professor').val();
			var senha_Professor = $('#senha_Professor').val();
			var id_Escola = $('#id_Escola').children(":selected").val();
			
			console.log('tipo'+id_Professor);
			console.log('tipo'+nome_Professor);
			console.log('tipo'+email_Professor);
			console.log('tipo'+senha_Professor);
			console.log('tipo'+id_Escola);
			
			//2 validar os inputs
			if(nome_Professor === "" || email_Professor === "" || senha_Professor === "" || id_Escola === ""){
				return alert('Todos os campos com asterisco (*) devem ser preenchidos!!');
			}
			
			else{
				var emailtester = false;
				var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
				emailtester = re.test(email_Professor);

				if(!emailtester){
					return alert("Formato de email incorreto. Corrija o campo e tente novamente");
				}
				
				$.ajax({
					   url: 'engine/controllers/professor.php',
					   data: {
							id_Professor : null,
							nome_Professor : nome_Professor,
							email_Professor : email_Professor,
							senha_Professor : senha_Professor,
							id_Escola : id_Escola,
							action: 'create'
					   },
					   error: function(jqXHR, exception) {
							alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							//cosole.log(msg);
					   },
					   success: function(data) {
							//console.log(data);							
							if($.trim(data) === "true"){
							}
							else{
								alert('Algum erro ocorreu e o salvamento pode ter sido mal sucedido.');	
							}
					   },
					   
					   type: 'POST'
					});	
			}
			
			//3 transferir os dados dos inputs para o arquivo q ira tratar
			
			//4 observar a resposta, e falar pra usuario o que aconteceu
		});
		
		
		
		
		//mascaras abaixo
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<html>
<body>
<br>

<ol class="breadcrumb" style="font-family:'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li><a id="Voltar1">Entrar</a></li>
    <li class="active">Cadastre-se</li>
</ol>

<h1 style="font-family:'Times New Roman', Times, serif" align="center">
	Cadastre-se
</h1>

<br>
<br>

<div class="row" align="center">
    	<div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Nome Completo *</span>
 			<input id="nome_Professor" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Email *</span>
 			<input id="email_Professor" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Senha *</span>
            <input id="senha_Professor" type="password" class="form-control" placeholder="" aria-describedby="basic-addon1">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Escola *</span>
            <select id="id_Escola" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
 			<option value="0">Selecione a escola</option>
                <?php 
					
					$Escolas = new Escola();
					$Escolas = $Escolas->ReadAll();
					foreach($Escolas as $Escola){
						?>
                        	<option value="<?php echo $Escola['id_Escola']; ?>"><?php echo $Escola['nome_Escola']; ?></option>
                        <?php
					}
				?>  
            </select>
		</div>
    </div>
<br><br>

<div align="center">
    <button id="Salvar" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
    	Salvar
    </button>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cadastro realizado com sucesso!</h4>
      </div>
      <div class="modal-footer">
        <button id="Login" type="button" class="btn btn-default" >OK</button>
      </div>
    </div>

  </div>
</div>

<br>
<br>
<div align="center">
<li style="font-family:Georgia, 'Times New Roman', Times, serif">*: campo de preenchimento obrigatório.</li>
</div>


<div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
</body>
</html>