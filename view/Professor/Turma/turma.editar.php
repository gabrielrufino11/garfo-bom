<script>
	$(document).ready(function(e) {
		
		var config = {
				'.chosen-select'           : {width:"100%", no_results_text:'Nenhum Morador com nome'},
				'.chosen-select-deselect'  : {allow_single_deselect:true},
				'.chosen-select-no-single' : {disable_search_threshold:10},
				'.chosen-select-no-results': {no_results_text:'Nenhum Morador com nome'},
				'.chosen-select-width'     : {width:"100%"}
			}
			for (var selector in config) {
				$(selector).chosen(config[selector]);
			}
			
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/Turma/turma.lista.php');	
		});
		
		$('#AreadoProfessor').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#VerMinhasTurmas').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/Turma/turma.lista.php');
		});
		
		$("#myModal").on("hidden.bs.modal", function () {
	
			$('#loader').load('view/Professor/Turma/turma.lista.php');
		});
		
		$('#ListaTurmas').click(function(e) {
			
			//loader
			$('#myModal').modal('hide');			
    		
		});
		
		$('#Salvar').click(function(e) {
			e.preventDefault();
			
			//1 instansciar e recuperar valores dos inputs
			var id_Turma = $('#id_Turma').val();
			var nome_Turma = $('#nome_Turma').val();
			var id_Ano = $('#id_Ano').val();
			var id_Escola = $('#id_Escola').val();
			var id_Disciplina = $('#id_Disciplina').val();
			var id_Aluno = $('#id_Aluno').val();
			
			//2 validar os inputs
			if(id_Turma === "" || nome_Turma === "" || id_Ano === "" || id_Escola === "" || id_Disciplina === "" || id_Aluno === ""){
				return alert('Todos os campos com asterisco (*) devem ser preenchidos!!');
			}
			else{
				$.ajax({
					   url: 'core/controle/turma.php',
					   data: {
							id_Turma : id_Turma,
							nome_Turma : nome_Turma,
							id_Ano : id_Ano,
							id_Escola : id_Escola,
							id_Disciplina : id_Disciplina,
							action: 'update'
					   },
					   error: function(jqXHR, exception) {
							alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							//cosole.log(msg);
					   },
					   success: function(data) {
						   
							var data = $.parseJSON(data);
					
							if(typeof data.id_Turma != "undefined"){
				
								ids_alunos=$("#id_Aluno").chosen().val();

								for(var i=0;i<ids_alunos.length;i++){
									updateAluno(ids_alunos[i],data);
								}

								//colocar mensagem de sucesso, etc
								//$('#loader').load('view/Turma/turma.lista.php');
							}					
							else{
								alert('Algum erro ocorreu e o salvamento pode ter sido mal sucedido.');
							}
					   },
					   
					   type: 'POST'
					});	
			}
			
			//3 transferir os dados dos inputs para o arquivo q ira tratar
			
			//4 observar a resposta, e falar pra usuario o que aconteceu
		});
		
		
		
		
		//mascaras abaixo
	});
	
	$(".chosen-select").chosen();
	$("#id_Aluno").chosen().val();
	$("#meet_participants").chosen().val(["hola", "mundo", "cruel"]);
</script>

<?php
	require_once "../../../engine/config.php";
?>

<html>
<body>
<br>
<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="AreadoProfessor">Área do Professor</a></li>
    <li class="active"><a id="VerMinhasTurmas">Ver Minhas Turmas</a></li>
    <li class="active">Editar Minha Turma</li>
</ol>

<br>

<h1 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Editar Minha Turma
</h1>

<br>
<br>
<?php
	$Turma = new Turma();
	$Turma = $Turma->Read($_POST['id_Turma']);
	
?>

	<div class="row" align="center">
    	<div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Nome da Turma *</span>
 			<input id="nome_Turma" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="<?php echo $Turma['nome_Turma'];?>">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Ano Escolar *</span>
			 
 			<select id="id_Ano" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
            	<option value="0">Selecione o ano</option>
            	<?php
					$Anos = new Ano();
					$Anos = $Anos->ReadAll();
					
					foreach($Anos as $Ano){
						?>
                        	<option value="<?php echo $Ano['id_Ano']; ?>" <?php if($Turma['id_Ano'] === $Ano['id_Ano']){echo 'selected';} ?>><?php echo $Ano['nome_Ano']; ?></option>
                        <?php
					}
				?>   
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Escola *</span>
            <select id="id_Escola" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
 			<option value="0">Selecione a escola</option>
                <?php
					$Escola = new Escola();
					$Escolas = $Escola->ReadAll();
					foreach($Escolas as $Escola){
						?>
                        	<option value="<?php echo $Escola['id_Escola']; ?>" <?php if($Turma['id_Escola'] === $Escola['id_Escola']){echo 'selected';} ?>><?php echo $Escola['nome_Escola']; ?></option>
                        <?php
					}
				?>
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Disciplina *</span>
            <select id="id_Disciplina" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
 			<option value="0">Selecione a disciplina</option>
                <?php
					$Disciplina = new Disciplina();
					$Disciplinas = $Disciplina->ReadAll();
					foreach($Disciplinas as $Disciplina){
						?>
                        	<option value="<?php echo $Disciplina['id_Disciplina']; ?>" <?php if($Turma['id_Disciplina'] === $Disciplina['id_Disciplina']){echo 'selected';} ?>><?php echo $Disciplina['nome_Disciplina']; ?></option>
                        <?php
					}
				?>
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Alunos</span>
           		<select id="id_Aluno" type="text" multiple class="chosen-select" placeholder="" aria-describedby="basic-addon1" tabindex="2">
                <option value=""></option>
            	<?php
					$Aluno = new Aluno();
					$Alunos = $Aluno->ReadAll();
					if(!empty($Alunos)) {
					foreach($Alunos as $Aluno){
						?>
                        	<option value="<?php echo $Aluno['id_Aluno']; ?>" <?php if($Aluno['id_Turma'] === $Turma['id_Turma']){echo " selected";} ?>><?php echo $Aluno['nome_Aluno']; ?></option>
                        <?php
					} }
				?> 
            	</select>
		</div>
    </div>
<br><br>

<div align="center">
    <button id="Salvar" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
    	Salvar
    </button>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Turma editada com sucesso!</h4>
      </div>
      <div class="modal-footer">
        <button id="ListaTurmas" type="button" class="btn btn-default" >Ver Minhas Turmas</button>
      </div>
    </div>

  </div>
</div>


<br>
<div align="center">
<li style="font-family:Georgia, 'Times New Roman', Times, serif">*: campo de preenchimento obrigatório.</li>
</div>

<br>

<div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>

<br>

</body>
</html>