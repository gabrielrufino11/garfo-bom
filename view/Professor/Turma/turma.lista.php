<script>
	$(document).ready(function(e) {
        $('#Atualizar').click(function(e) {
			e.preventDefault();
    		$('#loader').load('view/Professor/Turma/turma.lista.php');	
		});
		$('#Voltar').click(function(e) {
			e.preventDefault();
    		$('#loader').load('view/Professor/areadoprofessor.php');	
		});
		
		$('#AlunosCadastrados').click(function(e) {
			e.preventDefault();
    		$('#loader').load('view/Aluno/aluno.lista.php');	
		});
		
		$('#Adicionar').click(function(e) {
			e.preventDefault();
    		$('#loader').load('view/Professor/Turma/turma.adicionar.php');	
		});
		$('.EditarItem').click(function(e) {
            e.preventDefault();
			var id_Turma = $(this).attr('id');
			$('#loader').load('view/Professor/Turma/turma.editar.php', {id_Turma : id_Turma});
        });
		$('#AreadoProfessor').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		$('.ExcluirItem').click(function(e) {
            e.preventDefault();
			var id_Turma = $(this).attr('id');
			if(confirm("Tem certeza que deseja excluir este dado?")){
				$.ajax({
					   url: 'engine/controllers/turma.php',
					   data: {
							id_Turma : id_Turma,
							action: 'delete'
					   },
					   error: function(jqXHR, exception) {
							alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							//cosole.log(msg);
					   },
					   success: function(data) {
							console.log(data);							
							if($.trim(data) === "true"){
								alert('Turma deletada com sucesso!');
    							$('#loader').load('view/Professor/Turma/turma.lista.php');	
							}
							else{
								alert('Algum erro ocorreu e a remocão pode ter sido mal sucedido.');	
							}
					   },
					   
					   type: 'POST'
					});	
			}
			
        });

	/*	$('#tabelaTurma').DataTable({
			"language": {
				"decimal":        "",
				"emptyTable":     "Nenhum dado disponível para exibição",
				"info":           "Mostrando _START_ de _END_ de _TOTAL_ resultados",
				"infoEmpty":      "Mostrando 0 de 0 de 0 resultados",
				"infoFiltered":   "(filtrado de _MAX_ resultados)",
				"infoPostFix":    "",
				"thousands":      ",",
				"lengthMenu":     "Mostrando _MENU_ resultados",
				"loadingRecords": "Carregando...",
				"processing":     "Processando...",
				"search":         "Buscar:",
				"zeroRecords":    "Nenhum resultado encontrado",
				"paginate": {
					"first":      "Primeiro",
					"last":       "Último",
					"next":       "Próximo",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": ativar ordenação crescente",
					"sortDescending": ": ativar ordenação decrescente"
				}
			}
		
		});*/
    });  
</script>

<?php
	require_once "../../../engine/config.php";
?>

<br>
<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="AreadoProfessor">Área do Professor</a></li>
    <li class="active">Ver Minhas Turmas</li>
</ol>

<h1 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Ver Minhas Turmas
</h1>

<br>

<div class="container" role="group"  aria-label="...">
	<button id="Atualizar" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
    	Atualizar
    </button>
	<button id="Adicionar" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    	Cadastrar Turma
    </button>
    <button id="AlunosCadastrados" type="button" class="btn btn-success" style="background-color:#C30">
    	Alunos Cadastrados
    </button>
</div>

<br>
<br>

<div class="container">

<?php
	
	$Turmas = new Turma();
	$Turmas = $Turmas->ReadAll();
	

	if(empty($Turmas)){
		?>
        	<section class="well">
            	<h4>Nenhum dado encontrado.</h4>
            </section>
        <?php
	}
	else{
		?>
            <table class="table text-striped table-hover" id="tabelaTurma">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Ano Escolar</th>
                        <th>Escola</th>
                        <th>Disciplina</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						$num = 0;
                    	foreach($Turmas as $Turma){
							$n++;
					?>    
                    <tr>
                        <td><?php echo $n ?></td>
                        <td><?php echo $Turma['nome_Turma']; ?></td>
                        <td><?php 
								$Ano = new Ano();
								$Ano = $Ano->Read($Turma['id_Ano']);
								echo $Ano['nome_Ano'];
							?></td>
                        <td><?php 
								$Escola = new Escola();
								$Escola = $Escola->Read($Turma['id_Escola']);
								echo $Escola['nome_Escola'];
							?></td>
                        <td><?php 
								$Disciplina = new Disciplina();
								$Disciplina = $Disciplina->Read($Turma['id_Disciplina']);
								echo $Disciplina['nome_Disciplina'];
							?></td>
                        <td class="align-center " >
                        	<button type="button" class="btn btn-warning EditarItem" id="<?php echo $Turma['id_Turma']; ?>">
                            	<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
    							Editar
    						</button>
                        </td>
                       <td class="align-center" >
                        	<button type="button" class="btn btn-danger btnExcluir ExcluirItem" id="<?php echo $Turma['id_Turma']; ?>">
                            	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
    							Deletar
    						</button>
                        </td>
                        
                    </tr>
                    <?php
						}
					?>   
                </tbody>    
            </table>
		<?php	
    }
	?>
    
</div>    
    
<br>    
	
<div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>