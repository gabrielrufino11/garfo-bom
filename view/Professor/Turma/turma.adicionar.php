<html>
<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#AreadoProfessor').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#AreadoProfessor1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#VerMinhasTurmas').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/Turma/turma.lista.php');
		});
		
		$("#myModal").on("hidden.bs.modal", function () {
	
			$('#loader').load('view/Professor/Turma/turma.lista.php');
		});
		
		$('#ListaTurmas').click(function(e) {
			
			//loader
			$('#myModal').modal('hide');			
    		
		});
	
		function updateAluno(id_Aluno,data){
//console.log(data);
			var id_Ano = data.id_Ano;
			var id_Escola = data.id_Escola;
			var id_Turma = data.id_Turma;
			var id_Disciplina = data.id_Disciplina;

			//console.log(id_aluno,id_Ano,id_Escola,id_Turma,id_Disciplina);
			$.ajax({
					   url: 'engine/controllers/aluno.php',
					   data: {
							id_Aluno : id_Aluno,
							id_Ano : id_Ano,
							id_Escola : id_Escola,
							id_Disciplina : id_Disciplina,
							id_Turma: id_Turma,
							action: 'updateTurma'
					   },
					   error: function(jqXHR, exception) {
							alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');

							//cosole.log(msg);
					   },
					   success: function(data) {
							console.log(data);
							if(data=='true'){
								//alert("Salvou");
							}					
							else{
								alert('Algum erro ocorreu e o salvamento pode ter sido mal sucedido.');
							}
					   },
					   
					   type: 'POST'
					});	
		}
	
		$('#Salvar').click(function(e){	
			//1 instansciar e recuperar valores dos inputs
			
			var nome_Turma =$('#nome_Turma').val();
			var id_Ano = $('#id_Ano').children(":selected").val();
			var id_Escola = $('#id_Escola').children(":selected").val();
			var id_Disciplina = $('#id_Disciplina').children(":selected").val();

			//2 validar os inputs
			if(nome_Turma === "" || id_Ano === "" || id_Escola === "" || id_Disciplina === "" ){
				return alert('Todos os campos com asterisco (*) devem ser preenchidos!!');
			}
			else{
				$.ajax({
					   url: 'engine/controllers/turma.php',
					   data: {
							id_Turma : null,
							nome_Turma : nome_Turma,
							id_Ano : id_Ano,
							id_Escola : id_Escola,
							id_Disciplina : id_Disciplina,
							action: 'createForAddStudents'
					   },
					   error: function(jqXHR, exception) {
							alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							//cosole.log(msg);
					   },
					   success: function(data) {
							
							var data = $.parseJSON(data);
					
							if(typeof data.id_Turma != "undefined"){
				
								ids_alunos=$("#id_Aluno").chosen().val();

								for(var i=0;i<ids_alunos.length;i++){
									updateAluno(ids_alunos[i],data);
								}

								//colocar mensagem de sucesso, etc
								//$('#loader').load('view/Turma/turma.lista.php');
							}					
							else{
								alert('Algum erro ocorreu e o salvamento pode ter sido mal sucedido.');
							}
					   },
					   
					   type: 'POST'
					});	
			}
			
	});
	
		var config = {
                  '.chosen-select'           : {},
                  '.chosen-select-deselect'  : {allow_single_deselect:true},
                  '.chosen-select-no-single' : {disable_search_threshold:10},
                  '.chosen-select-no-results': {no_results_text:'Oops, nada encontrado!'},
                  '.chosen-select-width'     : {width:"95%"}
              }
              for (var selector in config) {
                  $(selector).chosen(config[selector]);
              }
	
		
		
		//mascaras abaixo
		$('#atualizar').click(function(e){
			$('#loader').load('view/Professor/Turma/turma.adicionar.php');
		});



	});
	
	$(".chosen-select").chosen();
	$("#id_Aluno").chosen().val();
	
</script>

<?php
	require_once "../../../engine/config.php";
?>

<body>
<br>
<ol class="breadcrumb">
	<li><a href="index.php">Página Inicial</a></li>
    <li><a id="AreadoProfessor">Área do Professor</a></li>
    <li class="active">Criar Turma</li>
</ol>

<h1 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Criar Turma
</h1>
<br>
<br>
	<div class="row" align="center">
    	<div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Nome da Turma *</span>
 			<input id="nome_Turma" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Ano Escolar *</span>
 			<select id="id_Ano" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
            	<option value="0">Selecione o ano</option>
            	<?php 
					$Anos = new Ano(); 
          			$Anos = $Anos->ReadAll(); 
					foreach($Anos as $Ano){
						?>
                        	 <option value="<?php echo $Ano['id_Ano']; ?>"><?php echo $Ano['nome_Ano']; ?></option> 
                        <?php
					}
				?>     
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Escola *</span>
            <select id="id_Escola" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
 			<option value="0">Selecione a escola</option>
                <?php 
					$Escolas = new Escola(); 
          			$Escolas = $Escolas->ReadAll(); 
					foreach($Escolas as $Escola){
						?>
                        	 <option value="<?php echo $Escola['id_Escola']; ?>"><?php echo $Escola['nome_Escola']; ?></option> 
                        <?php
					}
				?> 
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Disciplina *</span>
            <select id="id_Disciplina" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
 			<option value="0">Selecione a disciplina</option>
                <?php 
					$Disciplinas = new Disciplina(); 
          			$Disciplinas = $Disciplinas->ReadAll(); 
					foreach($Disciplinas as $Disciplina){
						?>
                        	<option value="<?php echo $Disciplina['id_Disciplina']; ?>"><?php echo $Disciplina['nome_Disciplina']; ?></option>
                        <?php
					}
				?>  
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Alunos</span>
           		<select id="id_Aluno" type="text" multiple class="chosen-select" data-placeholder="Escolha os Alunos" aria-describedby="basic-addon1">
            	<option value="0" disabled>Escolha o(s) alunos(s)</option>
                <?php
					$Alunos = new Aluno(); 
          			$Alunos = $Alunos->ReadAll();
					foreach($Alunos as $Aluno){
						?>
                        	<option value="<?php echo $Aluno['id_Aluno']; ?>"><?php echo $Aluno['nome_Aluno']; ?></option>
                        <?php
					}
				?>           
            	</select>
		</div>	
    </div>
<br><br>

<div align="center">
    <button id="Salvar" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
    	Salvar
    </button>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Turma cadastrada com sucesso!</h4>
      </div>
      <div class="modal-footer">
        <button id="ListaTurmas" type="button" class="btn btn-default" >Ver Minhas Turmas</button>
      </div>
    </div>

  </div>
</div>

<br>
<br>
<div align="center">
<li style="font-family:Georgia, 'Times New Roman', Times, serif">*: campo de preenchimento obrigatório.</li>
</div>


<div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
<br>

</body>
</html>