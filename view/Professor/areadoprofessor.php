<?php session_start();

  if(empty($_SESSION)){
    ?>
    <script>
       $('#loader').load('view/Login/login.php');
    </script>
 <?php
 	}
	else {
		if ($_SESSION['type'] === 'aluno'){
			?>
            <script>
       		$('#loader').load('view/Login/login.php');
    		</script>
            <?php
		}
	}
 ?>
<script>
$('#navbar').load('navbar.php');
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('index.php');
		});
		
		$('#MinhaBiblioteca').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/minhabiblioteca.php');
		});
		
		$('#MinhaBiblioteca1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/minhabiblioteca.php');
		});
		
		$('#CriarTurma').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/aluno.adicionar.php');
		});
		
		$('#VerMinhasTurmas').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/Turma/turma.lista.php');
		});
		
		$('#VerMinhasTurmas1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/Turma/turma.lista.php');
		});
		
		$('#AreadoAluno').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#AreadoAluno1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
	
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<head>
	<style>
	.btn1 {
		width:200px;
		right:940px;
		position:absolute;
	}
	.btn2 {
		width:200px;
		right:670px;
		position:absolute;
	}
	.btn3 {
		width:200px;
		right:400px;
		position:absolute;
	}
	.btn4 {
		width:200px;
		right:120px;
		position:absolute;
	}
	</style>
</head>

<body>
<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active">Área do Professor</li>
</ol>

<h1 align="center"  style="font-family:Georgia, 'Times New Roman', Times, serif">
	Área do Professor
</h1>

<br>

<h3 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Conte para mim o que você quer acessar, clicando no botão correspondente
</h3>

<br>
    
    <br><br><br><br><br>
    
    <div class="container" role="group" aria-label="...">
        <button id="MinhaBiblioteca" type="button" class="btn-lg btn-warning btn1" style="background-color:#C30">Minha Biblioteca</button>
        <button id="CriarTurma" type="button" class="btn-lg btn-warning btn2" style="background-color:#C30">Criar Turma</button>
        <button id="VerMinhasTurmas" type="button" class="btn-lg btn-warning btn3" style="background-color:#C30">Ver Minhas Turmas</button>
        <button id="AreadoAluno" type="button" class="btn-lg btn-warning btn4" style="background-color:#C30">Área do Aluno</button>
    </div>
    <br><br><br><br><br><br><br><br>
    
<div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
<br>

</body>
</html>
