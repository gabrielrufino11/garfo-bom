<?php session_start();

?>

<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('index.php');	
		});
		
		$('#AreadoProfessor').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#AreadoProfessor1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$("#myModal").on("hidden.bs.modal", function () {
	
			$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#Area').click(function(e) {
			
			//loader
			$('#myModal').modal('hide');			
    		
		});
		
		$('#Salvar').click(function(e) {
			e.preventDefault();
			
			//1 instansciar e recuperar valores dos inputs
			var id_Professor =$('#id_Professor').val();
			var nome_Professor =$('#nome_Professor').val();
			var email_Professor = $('#email_Professor').val();
			var senha_Professor = $('#senha_Professor').val();
			var id_Escola = $('#id_Escola').val();
			
			//2 validar os inputs
			if(id_Professor === "" || nome_Professor === "" || email_Professor === "" || senha_Professor === "" || id_Escola === ""){
				return alert('Todos os campos com asterisco (*) devem ser preenchidos!!');
			}
			else{
				$.ajax({
					   url: 'engine/controllers/professor.php',
					   data: {
							id_Professor : id_Professor,
							nome_Professor : nome_Professor,
							email_Professor : email_Professor,
							senha_Professor : senha_Professor,
							id_Escola : id_Escola,
							action: 'update'
					   },
					   error: function(jqXHR, exception) {
							alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							//cosole.log(msg);
					   },
					   success: function(data) {
							//console.log(data);							
							if($.trim(data) === "true"){
									
							}
							else{
								alert('Algum erro ocorreu e a ediçao pode ter sido mal sucedido.');	
							}
					   },
					   
					   type: 'POST'
					});	
			}
			
			//3 transferir os dados dos inputs para o arquivo q ira tratar
			
			//4 observar a resposta, e falar pra usuario o que aconteceu
		});
		
		
		
		
		//mascaras abaixo
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<html>
<body>
<br>
<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active">Editar Professor</li>
</ol>

<br>

<h1 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Editar Professor
</h1>

<br>
<br>
<?php
	$Professor = new Professor();
	$Professor = $Professor->Read($_SESSION['id_user']);
?>

	<div class="row" align="center">
    	<div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Nome Completo *</span>
 			<input id="nome_Professor" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="<?php echo $Professor['nome_Professor'];?>">
		</div>
        <br>
        <div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Email *</span>
 			<input id="email_Professor" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" value="<?php echo $Professor['email_Professor'];?>">
		</div>
        <br>
        <div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Senha *</span>
 			<input id="senha_Professor" type="password" class="form-control" placeholder="" aria-describedby="basic-addon1" value="<?php echo $Professor['senha_Professor'];?>">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Escola *</span>
            <select id="id_Escola" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
 			<option value="0">Selecione a escola</option>
                <?php
					$Escola = new Escola();
					$Escolas = $Escola->ReadAll();
					foreach($Escolas as $Escola){
						?>
                        	<option value="<?php echo $Escola['id_Escola']; ?>" <?php if($Professor['id_Escola'] === $Escola['id_Escola']){echo 'selected';} ?>><?php echo $Escola['nome_Escola']; ?></option>
                        <?php
					}
				?>
            </select>
		</div>
        <br>
    </div>
<br><br>

<div align="center">
    <button id="Salvar" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
    	Salvar
    </button>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Professor editado com sucesso!</h4>
      </div>
      <div class="modal-footer">
        <button id="Area" type="button" class="btn btn-default" >OK</button>
      </div>
    </div>

  </div>
</div>

<br>
<br>
<div align="center">
<li style="font-family:Georgia, 'Times New Roman', Times, serif">*: campo de preenchimento obrigatório.</li>
</div>


<div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>

</body>
</html>