<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
	
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<body>
<br>
<br>
<br>

   	<div class="container">
    	<h1 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">Minha Biblioteca</h1>
        <br><br>
        <h4 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">Em breve estará disponível o material didático para o professor</h4>
    </div>
    
    <br><br>
    <div class="container">
    	<h4 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif"><a>PDF do Material 1</a></h4>
        <h4 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif"><a>PDF do Material 2</a></h4>
        <h4 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif"><a>PDF do Material 3</a></h4>
    
    <br><br><br>
    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
    

</body>
</html>
