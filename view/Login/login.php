<?php session_start();
	if(!empty($_SESSION)){
		switch ($_SESSION['type']){
		 	case 'professor':
			?>
			<script>$('#loader').load('view/Professor/areadoprofessor.php');</script>
			<?php
			break;	
			
			case 'aluno':
			?>
			<script> $('#loader').load('view/Aluno/areadoaluno.php'); </script>
			<?php
            break;
		}
	}
    
 ?>

<script>

	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('index.php');
		});
	});

</script>


	<br>
	<h1 style="font-family:'Times New Roman', Times, serif" align="center">
	Entrar
	</h1>
	
    <br>
    
    <h3 style="font-family:'Times New Roman', Times, serif" align="center">
	Se for professor, entre com seu email. Se for aluno, entre com a seu nome.
	</h3>
    <h3 style="font-family:'Times New Roman', Times, serif" align="center">
	Lembrando que só os professores têm acesso à Área do Professor.
	</h3>
    
    <br>
    
    <main class="container-fluid" id="loader_login">
	<div class="row" align="center">
    	<div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Nome / Email *</span>
 			<input id="email_login" type="text" class="form-control" placeholder="Email" aria-describedby="basic-addon1">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Senha *</span>
            <input type="password" class="form-control" id="senha_login" placeholder="Senha" aria-describedby="basic-addon1">
		</div>
        <br>
        
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Escolha como deseja entrar no site *</span>
            <select id="Login" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
                <option value="0">Como quer entrar</option>
                <option value="professor">Professor</option>
                <option value="aluno">Aluno</option>
            </select>
		</div>

        <br><br>
        
        <div class="container" role="group"  aria-label="...">
		<button id="cadastro" type="button" class="btn btn-warning" style="background-color:#C30">
    	Cadastre-se
    	</button>
        <button class="btn btn-success" id="Logar">Entrar</button>
		</div>
    </div>
    
    <br><br>
    
    <div align="center">
			<li style="font-family:Georgia, 'Times New Roman', Times, serif">É aluno e ainda não está cadastrado? Peça ao seu professor para te cadastrar.</li>
	</div>
    
    <br>
    
    <div class="container-fluid">
    	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    	</button>
  	</main>

    <script src="js/login.js"></script>
