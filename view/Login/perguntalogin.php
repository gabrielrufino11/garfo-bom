<?php session_start();

  if(!empty($_SESSION)){
    ?>
    <script>
       $('#loader').load('view/Aluno/areadoaluno.php');
    </script>
 <?php
 	}
?>
<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('index.php');
		});
		
		$('#AreadoAluno').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#LoginAluno').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Login/login.php');
		});
	
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<head>
	<style>
	.btn1 {
		width:220px;
		right:660px;
		position:absolute;
	}
	.btn2 {
		width:220px;
		right:360px;
		position:absolute;
	}
	</style>
</head>

<body>
<br>
<br>
<br>

   	<div class="container">
    	<h3 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">Olá amiguinho! Aqui você pode escolher se deseja acessar o site utilizando</h3>
        <h3 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">o Usuário e Senha que seu professor cadastrou para você ou não.</h3>
        <h3 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">Você poderá acessar normalmente o site, mas a sua pontuação no Jogo</h3>
        <h3 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif"> só será salva se você entrar com seu Usuário e Senha.</h3>
        <br>
        <h3 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">Deseja entrar com seu Usuário e Senha?</h3>
        <br><br>
         
    </div>
    <br>
    
    <div class="container" role="group" aria-label="...">
    	<button id="AreadoAluno" type="button" class="btn btn-warning btn1" style="background-color:#C30">Não</button>
        <button id="LoginAluno" type="button" class="btn btn-warning btn2" style="background-color:#C30">Sim</button>
    </div>
    
    <br><br><br>
    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
    

</body>
</html>
