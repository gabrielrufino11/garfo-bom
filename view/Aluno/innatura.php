<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#Voltar2').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Categoria').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#Oleos').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/oleos.php');
		});
		
		$('#QuizInNatura').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/Quiz/quizinnatura.php');
		});
	
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<head>
	<style>
	div1 {
		right:80px;
		position:absolute;
	}
	div2 {
		right:600px;
		position:absolute;
	}
	</style>
</head>
<body>
<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar2">Área do Aluno</a></li>
    <li class="active"><a id="Voltar1">Categoria de Alimentos</a></li>
    <li class="active">Alimentos In Natura ou Pouco Processados</li>
</ol>

<h2 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Alimentos In Natura ou Pouco Processados
</h2>
	
    <div class="container" role="group"  aria-label="...">
	<div1>
    	<button id="Oleos" type="button" class="btn btn-warning" style="background-color:#C30">Óleos, Gorduras, Sal e Açucar<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
    </div1>
	</div>
<br><br>

	
    <!--<div align="center" class="video-container">
		<iframe width="425" height="350" src="//www.youtube.com/embed/UgOfaalUVaY" frameborder="0" allowfullscreen></iframe>
	</div>-->
    
    <div align="center" class="video-container">
		<video controls src="videos/alimentoinnatura.mp4" width="550" height="300"></video>
	</div>
    
    <br>
    
    <div class="container" role="group"  aria-label="...">
    	<div2>
    	<button id="QuizInNatura" type="button" class="btn btn-warning" style="background-color:#C30">Quiz</button>
    	</div2>
        <button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    	</button>
	</div>

</body>
</html>
