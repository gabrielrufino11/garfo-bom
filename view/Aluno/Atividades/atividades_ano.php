<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/Atividades/atividades.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Voltar2').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/Atividades/atividades.php');
		});
		
		$('#adicao').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/Atividades/adicao.php');
		});
		
		$('#adicao1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/Atividades/adicao.php');
		});
	
	});
</script>

<?php
	require_once "../../../engine/config.php";
?>

<html>
<head>
	<style>
	.img1 {
		right:250px;
		position:absolute;
	}
	.img2 {
		right:550px;
		position:absolute;
	}
	.img3 {
		right:850px;
		position:absolute;
	}
	.btn1 {
		width:200px;
		right:250px;
		position:absolute;
	}
	.btn2 {
		width:200px;
		right:550px;
		position:absolute;
	}
	.btn3 {
		width:200px;
		right:850px;
		position:absolute;
	}
	</style>
</head>

<body>
<br>

<ol class="breadcrumb" style="font-family:'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1">Área do Aluno</a></li>
    <li class="active"><a id="Voltar2">Atividades</a></li>
    <li class="active">Atividades 1º Ano</li>
</ol>

<h1 align="center" style="font-family:'Times New Roman', Times, serif">
	Atividades <?php echo $_POST['ano'];?>º Ano
</h1>

<h3 align="center" style="font-family:'Times New Roman', Times, serif">
	Vamos brincar com o conhecimento? Escolha a matéria:
</h3>

<h4 align="center" style="font-family:'Times New Roman', Times, serif">
	Está disponível a atividade de matemática do 1º Ano. As atividades referentes aos outros anos estarão disponíveis em breve.
</h4>

<br>
    
    <br><br><br><br><br><br>
    
    <div class="container" role="group" aria-label="...">
        <button id="" type="button" class="btn-lg btn-warning col-sm-2 btn3" style="background-color:#C30">Português</button>
        <button id="adicao" type="button" class="btn-lg btn-warning col-sm-2 btn2" style="background-color:#C30">Matemática</button>
        <button id="" type="button" class="btn-lg btn-warning col-sm-2 btn1" style="background-color:#C30">Geografia</button>
    </div>
    <br><br><br><br><br><br><br><br>
    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>

</body>
</html>
