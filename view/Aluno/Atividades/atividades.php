<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('.atividadesImg, .atividades').click(function(e) {
			e.preventDefault();
			var ano = $(this).attr('ano');
			//console.log(ano);
    		$('#loader').load('view/Aluno/Atividades/atividades_ano.php',{ano:ano});
		});

	
	});
</script>

<?php
	require_once "../../../engine/config.php";
?>

<html>
<head>
	<style>
	.img1 {
		right:70px;
		position:absolute;
	}
	.img2 {
		right:300px;
		position:absolute;
	}
	.img3 {
		right:530px;
		position:absolute;
	}
	.img4 {
		right:760px;
		position:absolute;
	}
	.img5 {
		right:990px;
		position:absolute;
	}
	.btn1 {
		width:200px;
		right:70px;
		position:absolute;
	}
	.btn2 {
		width:200px;
		right:300px;
		position:absolute;
	}
	.btn3 {
		width:200px;
		right:530px;
		position:absolute;
	}
	.btn4 {
		width:200px;
		right:760px;
		position:absolute;
	}
	.btn5 {
		width:200px;
		right:990px;
		position:absolute;
	}
	</style>
</head>

<body>
<br>

<ol class="breadcrumb" style="font-family:'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1">Área do Aluno</a></li>
    <li class="active">Atividades</li>
</ol>

<h1 align="center" style="font-family:'Times New Roman', Times, serif">
	Atividades
</h1>


<h3 align="center" style="font-family:'Times New Roman', Times, serif">
	Em que ano escolar você está?
</h3>

<h4 align="center" style="font-family:'Times New Roman', Times, serif">
	Está disponível a atividade de matemática do 1º Ano. As atividades referentes aos outros anos estarão disponíveis em breve.
</h4>

<br>
    
    <br><br><br><br>
    
    <div class="container" role="group" aria-label="...">
        <button  ano="1" type="button" class="btn-lg btn-warning col-sm-2 btn5 atividades" style="background-color:#C30">1º Ano</button>
        <button  ano="2" type="button" class="btn-lg btn-warning col-sm-2 btn4 atividades" style="background-color:#C30">2º Ano</button>
        <button  ano="3" type="button" class="btn-lg btn-warning col-sm-2 btn3 atividades" style="background-color:#C30">3º Ano</button>
        <button  ano="4" type="button" class="btn-lg btn-warning col-sm-2 btn2 atividades" style="background-color:#C30">4º Ano</button>
        <button  ano="5" type="button" class="btn-lg btn-warning col-sm-2 btn1 atividades" style="background-color:#C30">5º Ano</button>
    </div>
    <br><br><br><br><br><br><br><br>
    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>

</body>
</html>
