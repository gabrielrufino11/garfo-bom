

<?php
	require_once "../../../engine/config.php";
?>



	<style>
	#element1 {float:left;}
	#element2 {float:right;}
.hoverable {
    -webkit-transition: -webkit-box-shadow .25s;
    transition: -webkit-box-shadow .25s;
    transition: box-shadow .25s;
    transition: box-shadow .25s, -webkit-box-shadow .25s;
}

.container {
    width: 565px;
}
        
	</style>

<br>

<ol class="breadcrumb" style="font-family:'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1">Área do Aluno</a></li>
    <li class="active"><a id="Voltar2">Atividades</a></li>
    <li class="active">Atividades 1º Ano</li>
</ol>

<h1 align="center">Matemática</h1>

<br><br><br>



   <!-- <button id="atualizar">atualizar</button> -->


    <?php 
    
    function shuffle_assoc(&$array) {
        $keys = array_keys($array);

        shuffle($keys);

        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }


        $frutas = array("banana.png","beringela.png","cereja.png","laranja.png","morango.png","pera.png","tomate.png","uva.png");
        shuffle ( $frutas );
        $path = "images/frutas/";


        for ($i=0; $i < 4; $i++) { 
            $questoes[$i] = array('var1' => rand(5,15),'var2'=> rand(5,15));
            $questoes[$i]['respostas']['certa']=$questoes[$i]['var1']+$questoes[$i]['var2'];
            $questoes[$i]['respostas']['errada1']=$questoes[$i]['respostas']['certa']+rand(1,3);
           
            do{
                 $questoes[$i]['respostas']['errada2']=$questoes[$i]['respostas']['certa']+rand(1,3);
            }while( $questoes[$i]['respostas']['errada2']<0 ||  $questoes[$i]['respostas']['errada2']=== $questoes[$i]['respostas']['errada1']);
           
            shuffle_assoc($questoes[$i]['respostas']);
            $keys[$i] = array_keys($questoes[$i]['respostas']);

            $questoes[$i]['fruta1']=$frutas[$i];
            $questoes[$i]['fruta2']=$frutas[$i+4];
        }

     
       /*foreach($questoes as $questao){
                 echo '<br>';
        var_dump($questao);
        }*/
    

    ?>



<?php 
    $frutaCount=0;
    $questaoCount=0;
    foreach ($questoes as $key=> $questao) {

        ?>
            <div class="container" role="group" aria-label="...">
               
                <div id="element1"><h1 class="well-sm"><?php echo $questao['var1']; ?></h1></div>
                <div id="element1"><img src="<?php echo $path.$frutas[$frutaCount++];?>" width="70px" height="100px" class="well-sm"></div>
                <div id="element1"><h1 class="well-sm">+</h1></div>
                <div id="element1"><h1 class="well-sm"><?php echo $questao['var2']; ?></h1></div>
                <div id="element1"><img src="<?php echo $path.$frutas[$frutaCount++];?>"  width="70px" height="100px" class="well-sm"></div>
                <div id="element1"><h1 class="well-sm">=</h1></div>

                <?php 

                    foreach ($questao["respostas"] as $key=>$resposta) {
                        ?>
                            <button style=" border-radius: 10px;" resposta="<?php echo $key;?>"  atividade="<?php echo 'atividade'.$questaoCount; ?>"><h1 style="margin-top:10px;"><?php echo $resposta; ?></h1></button>
                        <?php
                    }
                ?>
            </div>
        <?php
        $questaoCount++;
    }

?>
    
   
<div align="center">
    <h1 class="well-sm">Pontos: <span id="pontos">0</span>/4</h1>
</div>
 
<br>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Respostas</h4>
      </div>
      <div class="modal-body">
        <p>Você acertou <?php echo $acertos;?> questões</p>
      </div>
      <div class="modal-footer">
        <button id="reload-atividade" type="button" class="btn btn-default" data-dismiss="modal">Tentar Novamente</button>
      </div>
    </div>

  </div>
</div>

<div align="center">
    <button id="atualizar" type="button" class="btn btn-success">
    	Tentar Novamente
    </button>
</div>
    
    <br><br>
    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
	</div>
<script>


    $(document).ready(function(e) {
        $('#Voltar').click(function(e) {
            e.preventDefault();
            //loader
            $('#loader').load('view/Aluno/Atividades/atividades.php');
        });
        
        $('#Voltar1').click(function(e) {
            e.preventDefault();
            //loader
            $('#loader').load('view/Aluno/areadoaluno.php');
        });
        
        $('#Voltar2').click(function(e) {
            e.preventDefault();
            //loader
            $('#loader').load('view/Aluno/Atividades/atividades.php');
        });



        $('#atualizar').click(function(e){
            $('#loader').load('view/Aluno/Atividades/adicao.php');
        });

        $('button[atividade]').click(function(e){
            var atividade= $(this).attr('atividade');
          
            var respostasAtividade = $('button[atividade='+atividade+']');
            
            
            for(var i=0;i<respostasAtividade.length;i++){
                $(respostasAtividade[i]).attr('disabled','disabled');
            }  

            var resposta = $(this).attr('resposta');
            if(resposta=='certa'){
                $(this).attr('class','btn btn-success');
                var pontos = parseInt($('#pontos')[0].innerText);
                $('#pontos')[0].innerText=++pontos;
            }
            else{
                $(this).attr('class','btn btn-danger');
            }
            

        });

    });
</script>
