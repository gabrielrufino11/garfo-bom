<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#AreadoProfessor').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/areadoprofessor.php');
		});
		
		$('#CadastroAluno').click(function(e) {
			
			$('#loader').load('view/Aluno/aluno.adicionar.php');
		});
		
		$("#myModal").on("hidden.bs.modal", function () {
	
			$('#loader').load('view/Professor/Turma/turma.adicionar.php');
		});
		
		$('#CriarTurma').click(function(e) {
			
			//loader
			$('#myModal').modal('hide');			
    		
		});
		
		$('#CriarTurma1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Professor/Turma/turma.adicionar.php');
		});
		
		$('#Salvar').click(function(e) {
			e.preventDefault();
			
			//1 instansciar e recuperar valores dos inputs
			
			var id_Aluno =$('#id_Aluno').val();
			var nome_Aluno =$('#nome_Aluno').val();
			var id_Ano = $('#id_Ano').children(":selected").val();
			var id_Escola = $('#id_Escola').children(":selected").val();
			var senha_Aluno = $('#senha_Aluno').val();
			
			console.log('tipo'+id_Aluno);
			console.log('tipo'+nome_Aluno);
			console.log('tipo'+id_Ano);
			console.log('tipo'+id_Escola);
			console.log('tipo'+senha_Aluno);
			
			//2 validar os inputs
			if(nome_Aluno === "" || id_Ano === "" || id_Escola === "" || senha_Aluno === ""){
				return alert('Todos os campos com asterisco (*) devem ser preenchidos!!');
			}
			else{
				$.ajax({
					   url: 'engine/controllers/aluno.php',
					   data: {
							id_Aluno : null,
							nome_Aluno : nome_Aluno,
							id_Ano : id_Ano,
							id_Escola : id_Escola,
							senha_Aluno : senha_Aluno,
							action: 'create'
					   },
					   error: function(jqXHR, exception) {
							alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							//cosole.log(msg);
					   },
					   success: function(data) {
							//console.log(data);							
							if($.trim(data) === "true"){
								/*alert('Aluno cadastrado com sucesso!');
    							$('#loader').load('view/Aluno/aluno.lista.php');*/
							}
							else{
								
							}
					   },
					   
					   type: 'POST'
					});	
			}
			
			//3 transferir os dados dos inputs para o arquivo q ira tratar
			
			//4 observar a resposta, e falar pra usuario o que aconteceu
		});
		
		
		
		
		//mascaras abaixo
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<html>
<body>
<br>

<ol class="breadcrumb">
	<li><a href="index.php">Página Inicial</a></li>
    <li><a id="AreadoProfessor">Área do Professor</a></li>
    <li class="active"><a href="">Cadastrar Aluno</a></li>
</ol>

    <div class="alert alert-danger alert-dismissible fade in" role="alert"> 
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
        <h4>Aviso!</h4> 
        <p>Se deseja inserir alunos em uma turma, primeiramente você deve cadastrá-los no sistema, por meio desta página. Você não é obrigado a cadastrar alunos para criar uma turma.</p>
        <p> 
        <button id="CriarTurma1" type="button" class="btn btn-danger">Criar Turma sem Alunos</button>
        </p> 
    </div>

<h1 style="font-family:'Times New Roman', Times, serif" align="center">
	Cadastrar Aluno
</h1>

<br>

<div class="row" align="center">
    	<div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Nome do Aluno *</span>
 			<input id="nome_Aluno" type="text" class="form-control" placeholder="Digite o nome e sobrenome do aluno" aria-describedby="basic-addon1">
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Ano Escolar *</span>
 			<select id="id_Ano" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
            	<option value="0">Selecione o ano escolar</option>
                <?php 
					
					$Anos = new Ano();
					$Anos = $Anos->ReadAll();
					foreach($Anos as $Ano){
						?>
                        	<option value="<?php echo $Ano['id_Ano']; ?>"><?php echo $Ano['nome_Ano']; ?></option>
                        <?php
					}
				?>            
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group">
  			<span class="input-group-addon" id="basic-addon1">Escola *</span>
            <select id="id_Escola" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
 			<option value="0">Selecione a escola</option>
                <?php 
					
					$Escolas = new Escola();
					$Escolas = $Escolas->ReadAll();
					foreach($Escolas as $Escola){
						?>
                        	<option value="<?php echo $Escola['id_Escola']; ?>"><?php echo $Escola['nome_Escola']; ?></option>
                        <?php
					}
				?>  
            </select>
		</div>
        <br>
        <div class="col-sm-4 input-group" class="float-none">
  			<span class="input-group-addon" id="basic-addon1">Senha *</span>
 			<input id="senha_Aluno" type="password" class="form-control" placeholder="" aria-describedby="basic-addon1">
		</div>
    </div>
<br><br>

<div align="center">
    <button id="Salvar" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
    	Salvar
    </button>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Aluno cadastrado com sucesso!</h4>
      </div>
      <div class="modal-body">
        <p>O que você gostaria de fazer em seguida</p>
      </div>
      <div class="modal-footer">
        <button id="CadastroAluno" type="button" class="btn btn-default" data-dismiss="modal">Cadastrar Mais Alunos</button>
        <button id="CriarTurma" type="button" class="btn btn-default" >Criar Turma</button>
      </div>
    </div>

  </div>
</div>

<br>
<br>
<div align="center">
<li style="font-family:Georgia, 'Times New Roman', Times, serif">*: campo de preenchimento obrigatório.</li>
</div>


<div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
<br>

</body>
</html>