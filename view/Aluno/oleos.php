<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/innatura.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#Voltar2').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Processados').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/processados.php');
		});
		
		$('#Categorias').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#QuizOleos').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/Quiz/quizoleos.php');
		});
	
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<head>
	<style>
	div1 {
		right:80px;
		position:absolute;
	}
	div2 {
		right:600px;
		position:absolute;
	}
	</style>
</head>
<body>
<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar2">Área do Aluno</a></li>
    <li class="active"><a id="Voltar1">Categoria de Alimentos</a></li>
    <li class="active">Óleos, Gorduras, Sal e Açucar</li>
</ol>

<h2 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Óleos, Gorduras, Sal e Açucar
</h2>

	<div class="container" role="group"  aria-label="...">	
     <button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Alimentos In Natura
    </button>
    <div1>
    	<button id="Processados" type="button" class="btn btn-warning" style="background-color:#C30">Alimentos Processados<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
   	</div1>
    </div>

<br>
	
    <div align="center" class="video-container">
		<video controls src="videos/alimentooleos.mp4" width="550" height="300"></video>
	</div>
    
    <br>
    
    <div class="container" role="group"  aria-label="...">
		<button id="Categorias" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        	Voltar
    	</button>
    	<div2>
    	<button id="QuizOleos" type="button" class="btn btn-warning" style="background-color:#C30">Quiz</button>
    	</div2>
	</div>

</body>
</html>
