<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/processados.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Voltar2').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#Voltar3').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/processados.php');
		});

                $('#questionario').on('submit', (function(e) {
            e.preventDefault();
		
			$.ajax({
			   url: 'view/Aluno/Quiz/questionarioJson.php',
			   type: 'POST',
			   data: new FormData(this),
			   contentType: false,
			   cache: false,
		   	   processData:false,
			   error: function() {
					alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
			   },
			   success: function(data) {
                       // console.log(data);
						
						$('#loader').load('view/Aluno/Quiz/processadorRespostas.php',
                        {
                            dados:data, //dados do formulario
                            //a partir daqui dados da página de resultados
                            title: "Quiz: Alimentos Processados",
                            voltar: "view/Aluno/processados.php",
                            voltar1: {
                                        location:"view/Aluno/areadoaluno.php",
                                        text: "Área do Aluno"
                                        },
                            voltar2: {
                                        location:"view/Aluno/categoriasalimentos.php",
                                        text: "Categoria de Alimentos"
                                        },
                            voltar3: {
                                        location:"view/Aluno/processados.php",
                                        text: "Alimentos Processados"
                                        },
                            tentar: "view/Aluno/Quiz/quizprocessados.php",

                           });
			
			   }
			   
			   
			});	
        }));

	});

</script>

<?php
	require_once "../../../engine/config.php";
?>

<head>
</head>
<body>
<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1">Área do Aluno</a></li>
    <li class="active"><a id="Voltar2">Categoria de Alimentos</a></li>
    <li class="active"><a id="Voltar3">Alimentos Processados</a></li>
    <li class="active">Quiz: Alimentos Processados</li>
</ol>

<h2 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Quiz: Alimentos Processados
</h2>

<br>

<div class="container">
    <form id="questionario" name ="questionario" method ="post" action="view/Aluno/Quiz/quizprocessados.resposta.php">
    
    <input type="hidden" name="questao1-certa" value="b">
    <label>1) O que são Alimentos Processados?</label>
    <p>
    <input type="radio" name="questao1" value="a" required/> A) São alimentos que vêm diretamente das plantas ou dos animais.
    <br>
    <input type="radio" name="questao1" value="b" /> B) São os alimentos fabricados pelas indústrias, que querem fazer os alimentos durarem mais tempo.
    <br>
    <input type="radio" name="questao1" value="c" /> C) São alimentos que não sofrem nenhuma alteração antes de serem consumidos.
    <br>
    <br>
    
    <input type="hidden" name="questao2-certa" value="a">
    <label>2) Quais dos Alimentos abaixo são Alimentos Processados?</label>
    <p>
    <input type="radio" name="questao2" value="a" required/> A) Pães feitos com farinha de trigo, atum enlatado e palmito.
    <br>
    <input type="radio" name="questao2" value="b" /> B) Refrigerantes, salsicha e bala.
    <br>
    <input type="radio" name="questao2" value="c" /> C) Cebola, Limão e Banana.
    <br>
    <br>
    
    <input type="hidden" name="qtdPerguntas" value="2">
    <button type="submit" name = "enviar" id="enviar">Enviar</button>
    </form>
    
    
</div>

<br>

    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>

</body>
</html>
