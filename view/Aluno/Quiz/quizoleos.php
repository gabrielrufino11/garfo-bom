<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/oleos.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Voltar2').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#Voltar3').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/oleos.php');
		});

                $('#questionario').on('submit', (function(e) {
            e.preventDefault();
		
			$.ajax({
			   url: 'view/Aluno/Quiz/questionarioJson.php',
			   type: 'POST',
			   data: new FormData(this),
			   contentType: false,
			   cache: false,
		   	   processData:false,
			   error: function() {
					alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
			   },
			   success: function(data) {
                       // console.log(data);
						
						$('#loader').load('view/Aluno/Quiz/processadorRespostas.php',
                        {
                            dados:data, //dados do formulario
                            //a partir daqui dados da página de resultados
                            title: "Quiz: Óleos, Gorduras, Sal e Açúcar",
                            voltar: "'view/Aluno/oleos.php",
                            voltar1: {
                                        location:"view/Aluno/areadoaluno.php",
                                        text: "Área do Aluno"
                                        },
                            voltar2: {
                                        location:"view/Aluno/categoriasalimentos.php",
                                        text: "Categoria de Alimentos"
                                        },
                            voltar3: {
                                        location:"view/Aluno/oleos.php",
                                        text: "Óleos, Gorduras, Sal e Açúcar"
                                        },
                            tentar: "view/Aluno/Quiz/quizoleos.php",

                           });
			
			   }
			   
			   
			});	
        }));

	});
</script>

<?php
	require_once "../../../engine/config.php";
?>

<head>
</head>
<body>
<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1">Área do Aluno</a></li>
    <li class="active"><a id="Voltar2">Categoria de Alimentos</a></li>
    <li class="active"><a id="Voltar3">Óleos, Gorduras, Sal e Açucar</a></li>
    <li class="active">Quiz: Óleos, Gorduras, Sal e Açucar</li>
</ol>

<h2 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Quiz: Óleos, Gorduras, Sal e Açucar
</h2>

<br>

<div class="container">
    <form id="questionario" name ="questionario" method ="post" action="view/Aluno/Quiz/quizoleos.resposta.php">
    
     <input type="hidden" name="questao1-certa" value="c">
    <label>1) Como os Óleos, Gorduras, Sal e Açúcar podem contribuir para nossa alimentação?</label>
    <p>
    <input type="radio" name="questao1" value="a" required/> A) Contribuem para tornar a comida menos saborosa.
    <br>
    <input type="radio" name="questao1" value="b" /> B) Constribuem para tornar a comida mais salgada.
    <br>
    <input type="radio" name="questao1" value="c" /> C) Contribuem para tornar a comida mais gostosa.
    <br>
    <br>
    
    <input type="hidden" name="questao2-certa" value="b">
    <label>2) Quais dos alimentos abaixo pertencem à categoria dos Óleos, Gorduras, Sal e Açúcar?</label>
    <p>
    <input type="radio" name="questao2" value="a" required/> A) Arroz, Feijão e Farinha.
    <br>
    <input type="radio" name="questao2" value="b" /> B) Manteiga, Açúcar branco e Sal de Cozinha.
    <br>
    <input type="radio" name="questao2" value="c" /> C) Mandioca, Batata e Abóbora.
    <br>
    <br>
    
    <input type="hidden" name="questao3-certa" value="a">
   	<label>3) Porque os Óleos, Gorduras, Sal e Açúcar devem ser usados em pequenas quantidades?</label>
    <p>
   	<input type="radio" name="questao3" value="a" required/> A) Porque fazem mal a saúde.
    <br>
    <input type="radio" name="questao3" value="b" /> B) Porque são muito caros.
    <br />
    <input type="radio" name="questao3" value="c" /> C) Porque fazem bem a saúde.
    <br>
    <br>
    
    <input type="hidden" name="qtdPerguntas" value="3">
    <button type="submit" name = "enviar" id="enviar">Enviar</button>
    </form>
    
    
</div>

<br>

    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>

</body>
</html>
