<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load("<?php echo $_POST['voltar']; ?>");
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load("<?php echo $_POST['voltar1']['location']; ?>");
		});
		
		$('#Voltar2').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load("<?php echo $_POST['voltar2']['location']; ?>");
		});
		
		$('#Voltar3').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load("<?php echo $_POST['voltar3']['location']; ?>");
		});
		
		$('#Tentar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load("<?php echo $_POST['tentar']; ?>");
		});
	});
</script>

<?php
	require_once "../../../engine/config.php";
    $dados=json_decode($_POST['dados'], true);
?>

    
 

<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1"></a><?php echo $_POST['voltar1']['text']; ?></li>
    <li class="active"><a id="Voltar2"><?php echo $_POST['voltar2']['text']; ?></a></li>
    <li class="active"><a id="Voltar3"><?php echo $_POST['voltar3']['text']; ?></a></li>
    <li class="active"><?php echo $_POST['title']; ?></li>
</ol>

<h2 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<?php echo $_POST['title']; ?>
</h2>

<br>

	<div class="container">
    <label>Respostas:</label><p>
	<?php

        for ($i=1; $i <= $dados['qtdPerguntas']; $i++){
            echo "<b>Questao ".$i."</b> = ".$dados['questao'.$i]."<br>";
        }
    ?>
    <br><br>
    <label>Meus acertos:</label><p>
    <?php
        $certo_errado="Errada";
        $respostas_certas=0;
        for ($i=1; $i <= $dados['qtdPerguntas']; $i++) { 
            if($dados['questao'.$i.'-certa']===$dados['questao'.$i]){
                $respostas_certas++;
                $certo_errado="Correta";
            }
            echo "<b>Questao ".$i."</b> = ".$certo_errado."<br>";
            $certo_errado="Errada";
        }

        $questao="questão";
        if($respostas_certas>1 ){
            $questao="questões";
        }
        

        echo "<br><h2><font color = red> Você acertou $respostas_certas $questao.</font></h2>";
    ?>
    
	<br><input type="button" value="Tentar Novamente" id="Tentar">
    

    <br><br>
    
    
    </div>
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
<br>

