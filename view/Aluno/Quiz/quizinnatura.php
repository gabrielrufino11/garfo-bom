<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/innatura.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Voltar2').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/categoriasalimentos.php');
		});
		
		$('#Voltar3').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/innatura.php');
		});
		
	/*	$('#Enviar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/Quiz/quizinnatura.resposta.php');
		});
*/
        $('#questionario').on('submit', (function(e) {
            e.preventDefault();
		
			$.ajax({
			   url: 'view/Aluno/Quiz/questionarioJson.php',
			   type: 'POST',
			   data: new FormData(this),
			   contentType: false,
			   cache: false,
		   	   processData:false,
			   error: function() {
					alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
			   },
			   success: function(data) {
                       // console.log(data);
						
						$('#loader').load('view/Aluno/Quiz/processadorRespostas.php',
                        {
                            dados:data, //dados do formulario
                            //a partir daqui dados da página de resultados
                            title: "Quiz: Alimentos In Natura ou Minimamente Processados",
                            voltar: "view/Aluno/Quiz/quizinnatura.php",
                            voltar1: {
                                        location:"view/Aluno/areadoaluno.php",
                                        text: "Área do Aluno"
                                        },
                            voltar2: {
                                        location:"view/Aluno/categoriasalimentos.php",
                                        text: "Categoria de Alimentos"
                                        },
                            voltar3: {
                                        location:"view/Aluno/innatura.php",
                                        text: "Alimentos In Natura ou Minimamente Processados"
                                        },
                            tentar: "view/Aluno/Quiz/quizinnatura.php",

                           });
			
			   }
			   
			   
			});	
        }));
		
	});
</script>

<?php
	require_once "../../../engine/config.php";
?>

<body>
<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1">Área do Aluno</a></li>
    <li class="active"><a id="Voltar2">Categoria de Alimentos</a></li>
    <li class="active"><a id="Voltar3">Alimentos In Natura ou Pouco Processados</a></li>
    <li class="active">Quiz: Alimentos In Natura ou Pouco Processados</li>
</ol>

<h2 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Quiz: Alimentos In Natura ou Pouco Processados
</h2>

<br>

<div class="container">

    <form id="questionario" name ="questionario" method="post" action="view/Aluno/Quiz/quizinnatura.resposta.php">
    
    <input type="hidden" name="questao1-certa" value="b">
    <label for="questao1">1) O que são alimentos In Natura?</label>
    <p>
    <input type="radio" name="questao1" value="a" required/> A) São alimentos produzidos na indústria.
    <br>
    <input type="radio" name="questao1" value="b" required/> B) São alimentos que vem diretamente das plantas ou dos animais.
    <br>
    <input type="radio" name="questao1" value="c" required/> C) São alimentos que não vem da natureza ou dos animais.
   
    <br>
    <br>
    <input type="hidden" name="questao2-certa" value="c">
    <label>2) Quais dos alimentos abaixo são Alimentos In Natura?</label>
    <p>
    <input type="radio" name="questao2" value="a" required/> A) Sal, Açúcar e Óleo de cozinha.
    <br>
    <input type="radio" name="questao2" value="b" required/> B) Biscoitos, Refrigerantes e Doces.
    <br>
    <input type="radio" name="questao2" value="c" required/> C) Frutas, Verduras e Ovos.
    <br>
    <br>
    
   	<input type="hidden" name="questao3-certa" value="a">
    <label>3) O que são alimentos Pouco Processados?</label>
    <p>
   	<input type="radio" name="questao3" value="a" required/> A) São alimentos que podem ter sofrido alguma alteração antes de serem consumidos.
    <br>
    <input type="radio" name="questao3" value="b" required/> B) São alimentos que não podem ser consumidos.
    <br />
    <input type="radio" name="questao3" value="c" required/> C) São alimentos que não sofrem nenhuma alteração antes de serem consumidos.
    <br>
    <br>
    
    <input type="hidden" name="questao4-certa" value="c">
    <label>4) Quais dos alimentos abaixo são Alimentos Pouco Processados?</label>
    <p>
    <input type="radio" name="questao4" value="a" required/> A) Biscoistos, Balas e Chocolate.
    <br>
    <input type="radio" name="questao4" value="b" required/> B) Refrigerantes, Doces e Batata Frita.
    <br>
    <input type="radio" name="questao4" value="c" required/> C) Arroz, Feijão, Leite e a Carne.
    <br>
    <br>
        
     <input type="hidden" name="qtdPerguntas" value="4">
     <br>
		<button type="submit" name = "enviar" id="enviar">Enviar</button>
   
    </form>
    
    
</div>

    <br>
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>
<br>

</body>
</html>
