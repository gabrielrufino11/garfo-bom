<script>
	$(document).ready(function(e) {
		$('#Voltar').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#Voltar1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/areadoaluno.php');
		});
		
		$('#InNatura').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/innatura.php');
		});
		
		$('#InNatura1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/innatura.php');
		});
		
		$('#Oleos').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/oleos.php');
		});
		
		$('#Oleos1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/oleos.php');
		});
		
		$('#Processados').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/processados.php');
		});
		
		$('#Processados1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/processados.php');
		});
		
		$('#Ultraprocessados').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/ultraprocessados.php');
		});
		
		$('#Ultraprocessados1').click(function(e) {
			e.preventDefault();
			//loader
    		$('#loader').load('view/Aluno/ultraprocessados.php');
		});
	
	});
</script>

<?php
	require_once "../../engine/config.php";
?>

<head>
	<style>
	.img1 {
		right:100px;
		width:200px;
		position:absolute;
	}
	.img2 {
		right:400px;
		width:200px;
		position:absolute;
	}
	.img3 {
		right:700px;
		width:200px;
		position:absolute;
	}
	.img4 {
		right:1000px;
		width:180px;
		position:absolute;
	}
	.btn1 {
		width:220px;
		right:980px;
		position:absolute;
	}
	.btn2 {
		width:220px;
		right:680px;
		position:absolute;
	}
	.btn3 {
		width:220px;
		right:380px;
		position:absolute;
	}
	.btn4 {
		width:220px;
		right:90px;
		position:absolute;
	}
	</style>
</head>

<body>
<br>

<ol class="breadcrumb" style="font-family:Georgia, 'Times New Roman', Times, serif">
	<li><a href="index.php">Página Inicial</a></li>
    <li class="active"><a id="Voltar1">Área do Aluno</a></li>
    <li class="active">Categoria de Alimentos</li>
</ol>

<h1 align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">
	Categoria de Alimentos
</h1>

<br>

<br>
    
     <div class="container" role="group" aria-label="...">
       	<img src="images/innatura.png" alt="Alimentos In Natura" class="col-sm-2 img4" id="InNatura">
        <img src="images/oleos.png" alt="Óleos, Gorduras, Sal e Açucar" class="col-sm-2 img3" id="Oleos">
        <img src="images/processados.png" alt="Alimentos Processados" class="col-sm-2 img2" id="Processados">
        <img src="images/ultraprocessados.png" alt="Alimentos Ultraprocessados" class="col-sm-2 img1" id="Ultraprocessados">
    </div>
    
    <br><br><br><br><br><br><br><br><br><br><br><br>
    
    <div class="container" role="group" aria-label="...">
        <div4><button id="InNatura1" class="btn btn-warning btn1" type="button" style="background-color:#C30">Alimentos In Natura</button></div4>
        <div3><button id="Oleos1"class="btn btn-warning btn2" type="button" style="background-color:#C30">Óleos, Gorduras, Sal e Açucar</button></div3>
        <div2><button id="Processados1" class="btn btn-warning btn3" type="button" style="background-color:#C30">Alimentos Processados</button></div2>
        <div1><button id="Ultraprocessados1" class="btn btn-warning btn4" type="button" style="background-color:#C30">Alimentos Ultraprocessados</button></div1>
    </div>
    <br><br><br><br><br>
    
    <div class="container" role="group"  aria-label="...">
	<button id="Voltar" type="button" class="btn btn-warning" style="background-color:#C30"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	Voltar
    </button>
</div>

</body>
</html>
