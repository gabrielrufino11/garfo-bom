<?php 
require_once "engine/config.php";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Garfo Bom</title>
        <link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
        <link type="text/css" rel="stylesheet" href="css/font-awesome.css"/>
        <link type="text/css" rel="stylesheet" href="css/GarfoBom.css"/>
        <link type="text/css" rel="stylesheet" href="css/jquery.dataTables.css"/>
        <link type="text/css" rel="stylesheet" href="css/chosen.css"/>
        <style>
			.imglogo {
			left:50px;
			width:80px;
			height:45px;
			position:absolute;
			}
		</style>
    </head>
    <body>
    
	<nav class="navbar navbar-default navbar-fixed-top " id="navbar" style="background-color: #C30;">

    </nav>
    
    <main class="container-fluid full" id="loader">
    
    <br><br><br><br><br><br><br>
    
    <div class="container">
    	<div style="height: 0px;" class="row hidden-xs top-spacer">
        </div>
        <div class="row hidden-sm hidden-xs">
        <div class="col-lg-7 col-md-8">
        <h1 data-i18n="new_home.slogan" align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">BEM VINDO AO GARFO BOM!</h1>
        <br><br>
        <h3 data-i18n="new_home.slogan" align="center" style="font-family:Georgia, 'Times New Roman', Times, serif">Ambiente Computacional para Apoio à Educação Alimentar Nutricional de Crianças do Ensino Fundamental I</h3>
        </div>
        <div class="col-lg-3 col-lg-offset-2 col-md-4">
        <div class="well text-center"><h6 id="classroom-edition-header" data-i18n="new_home.classroom_edition"></h6>
        <div><a id="pergunta_login_link"><button data-event-action="Homepage Click Teacher Button CTA" data-i18n="new_home.im_a_teacher" class="teacher-btn btn btn-primary btn-lg btn-block" style="background-color:#C30">Eu sou um Aluno(a)</button></a></div>
        <br><br>
        <div><a id="area_professor_link_1"><button data-event-action="Homepage Click Student Button CTA" data-i18n="new_home.im_a_student" class="student-btn btn btn-forest btn-lg btn-block" style="background-color:#CC6">Eu sou um Professor(a)</button></a></div>
        <br><br>
        <div><a id="jogo_link"><button data-event-action="Homepage Click Student Button CTA" data-i18n="new_home.im_a_student" class="student-btn btn btn-forest btn-lg btn-block" style="background-color:#6C3">Jogar Agora</button></a></div>
        </div>
        </div>
        </div>
    </div>
    
    </main>
   	
    <footer class="protocolofooter">
    	<p class="text-center"> Copyright © GARFO BOM 2017. Todos os direitos reservados. </p>
    </footer>
    
	<script src="js/jquery-3.1.0.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script src="js/garfo_bom.js"></script>
	<script src="js/menu.js"></script>
    <script src="js/jquery.dataTables.js"></script>
	<script src="js/moment.js"></script>
   	<script src="js/chosen.jquery.js"></script>
    


</body>
</html>
