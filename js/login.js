// JavaScript Document

 $(document).ready(function(e) {

        $('#Logar').click(function(e) {
        	var email = $('#email_login').val();
    		var senha = $('#senha_login').val();
			var option = $('#Login').val();
		

    		if(email === "" || senha === "" || typeof option === "undefined"){
    			return alert('Todos os campos são obrigatórios!');
    		}
    		else{
    			$.ajax({
    			   url: 'engine/controllers/login.php',
    			   data: {
    					email : email,
						senha : senha,
						option: option
    			   },

    			   error: function() {
    					alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
    			   },
    			   success: function(data) {
						
						data=$.parseJSON(data);
						//console.log(data, typeof data, data=="true");
						
    					if(data === 'true'){
							
							console.log(data);
							if (option === 'professor'){
								$('#loader').load('view/Professor/areadoprofessor.php');
							}
							else if (option === 'aluno'){
								$('#loader').load('view/Aluno/areadoaluno.php');
							}
							
							
    					}
    					else if(data === 'no_user_found'){
    						alert('Nenhum usuario encontrado com este Nome/Email.');
    					}
              else if(data === 'wrong_password'){
                alert('Senha errada');

              }
    					else{
    						alert('Erro ao conectar com banco de dados. Aguarde e tente novamente em alguns instantes.');
						}
						
    			   },

    			   type: 'POST'
    			});
    		}
        });

        $('#cadastro').click(function(e){
            e.preventDefault();

            	$('#loader').load('view/Professor/professor.adicionar.php');
         });

});
