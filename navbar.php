 <?php session_start();
 require_once 'engine/config.php';
 ?>
 
 <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!--<a class="navbar-brand" href="index.php" id="">GARFO BOM</a>-->
          <img src="images/logo.png" class="imglogo">
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <?php 
          if(!empty($_SESSION)){
              switch($_SESSION['type']){
                case "professor":
              
                    $user = new Professor(); 
                    $user = $user->Read($_SESSION['id_user']);
                    $nome = $user['nome_Professor'];
                break;
                
                case "aluno":
                    $user = new Aluno();
                    $user = $user->Read($_SESSION['id_user']);
                   
                    $nome = $user['nome_Aluno'];
                  
              
                break;
              }
          }
          ?>
          <ul class="nav navbar-nav navbar-right">
            <?php
            if(!empty($_SESSION)){
            	?>
          		<li><a<?php
					if ($_SESSION['type'] === 'professor') {?>
						id="professor"
				<?php }
					else if ($_SESSION['type'] === 'aluno') {?>
						id=""
				<?php }
					?>>Bem vindo, <?php echo $nome;?></a></li>
              <?php
          	}
            ?>
          	
            <li><a href="index.php" id=""><i class="fa fa-home" aria-hidden="true"></i> Página Inicial</a></li>
            
            <?php 
				if(empty($_SESSION)){
					?>
					<li><a href="" id="area_professor_link"><i class=" tooltip-arrow" aria-hidden="true"></i> Entrar </a></li>
					<?php
				}
				else{
					?>
                    <li><a href="#" id="sair"><i class="fa fa-sign-out" aria-hidden="true"></i> SAIR </a></li>
					<?php	
				}
			?>
            
              
            
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
      
      <script>
      
      $(document).ready(function(e) {
        
			$('#area_professor_link').click(function(e) {
			e.preventDefault();
			$('#loader').load('view/Login/login.php');
		});
		
			$('#professor').click(function(e) {
			e.preventDefault();
			$('#loader').load('view/Professor/professor.editar.php');
		});
		
		$('#sair').click(function(e) {
				e.preventDefault();
		        //loader
				$.ajax({
				   url: 'engine/controllers/logout.php',
				   data: {

				   },
				   error: function() {
						alert('Erro na conexão com o servidor. Tente novamente em alguns segundos.');
				   },
				   success: function(data) {
						if(data === 'kickme'){
							document.location.href = 'index.php';
						}


						else{
							alert('Erro ao conectar com banco de dados. Aguarde e tente novamente em alguns instantes.');
						}
				   },

				   type: 'POST'
					});
		    });
	
    });
      </script>
