/*
Navicat MySQL Data Transfer

Source Server         : mysql2
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : garfo-bom

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-05-05 15:59:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `aluno`
-- ----------------------------
DROP TABLE IF EXISTS `aluno`;
CREATE TABLE `aluno` (
  `id_aluno` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(50) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `sobrenome` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `pontuacao` int(11) NOT NULL,
  `id_turma` int(11) NOT NULL,
  PRIMARY KEY (`id_aluno`),
  KEY `fk_aluno_turma` (`id_turma`),
  CONSTRAINT `fk_aluno_turma` FOREIGN KEY (`id_turma`) REFERENCES `turma` (`id_turma`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aluno
-- ----------------------------

-- ----------------------------
-- Table structure for `ano`
-- ----------------------------
DROP TABLE IF EXISTS `ano`;
CREATE TABLE `ano` (
  `id_ano` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  `id_escola` int(11) NOT NULL,
  PRIMARY KEY (`id_ano`),
  KEY `fk_idescola_ano` (`id_escola`),
  CONSTRAINT `fk_idescola_ano` FOREIGN KEY (`id_escola`) REFERENCES `escola` (`id_escola`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ano
-- ----------------------------

-- ----------------------------
-- Table structure for `escola`
-- ----------------------------
DROP TABLE IF EXISTS `escola`;
CREATE TABLE `escola` (
  `id_escola` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id_escola`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of escola
-- ----------------------------

-- ----------------------------
-- Table structure for `professor`
-- ----------------------------
DROP TABLE IF EXISTS `professor`;
CREATE TABLE `professor` (
  `id_professor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `sobrenome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  PRIMARY KEY (`id_professor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of professor
-- ----------------------------

-- ----------------------------
-- Table structure for `prof_escola`
-- ----------------------------
DROP TABLE IF EXISTS `prof_escola`;
CREATE TABLE `prof_escola` (
  `id_escola` int(11) NOT NULL,
  `id_professor` int(11) NOT NULL,
  PRIMARY KEY (`id_escola`,`id_professor`),
  KEY `fk_idprofessor` (`id_professor`),
  CONSTRAINT `fk_idescola` FOREIGN KEY (`id_escola`) REFERENCES `escola` (`id_escola`) ON UPDATE CASCADE,
  CONSTRAINT `fk_idprofessor` FOREIGN KEY (`id_professor`) REFERENCES `professor` (`id_professor`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prof_escola
-- ----------------------------

-- ----------------------------
-- Table structure for `turma`
-- ----------------------------
DROP TABLE IF EXISTS `turma`;
CREATE TABLE `turma` (
  `id_turma` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_turma` varchar(10) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `id_ano` int(11) NOT NULL,
  `id_professor` int(11) NOT NULL,
  PRIMARY KEY (`id_turma`),
  KEY `fk_turma_ano` (`id_ano`),
  KEY `fk_turma_professor` (`id_professor`),
  CONSTRAINT `fk_turma_ano` FOREIGN KEY (`id_ano`) REFERENCES `ano` (`id_ano`) ON UPDATE CASCADE,
  CONSTRAINT `fk_turma_professor` FOREIGN KEY (`id_professor`) REFERENCES `professor` (`id_professor`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of turma
-- ----------------------------
