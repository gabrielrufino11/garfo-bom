<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class professor {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Professor;
		private $nome_Professor;
		private $email_Professor;
		private $senha_Professor;
		private $id_Escola;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Professor, $nome_Professor, $email_Professor, $senha_Professor, $id_Escola) { 
			$this->id_Professor = $id_Professor;
			$this->nome_Professor = $nome_Professor;
			$this->email_Professor = $email_Professor;
			$this->senha_Professor = $senha_Professor;
			$this->id_Escola = $id_Escola;
						
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Professor;
			$this->nome_Professor;
			$this->email_Professor;
			$this->senha_Professor;
			$this->id_Escola;
		}
		
		//destructor
		function __destruct() {
			$this->id_Professor;
			$this->nome_Professor;
			$this->email_Professor;
			$this->senha_Professor;
			$this->id_Escola;
		}
			
	};

?>
