<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class ano {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Ano;
		private $nome_Ano;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Ano, $nome_Ano) { 
			$this->id_Ano = $id_Ano;
			$this->nome_Ano = $nome_Ano;
						
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Ano;
			$this->nome_Ano;
		}
		
		//destructor
		function __destruct() {
			$this->id_Ano;
			$this->nome_Ano;
		}
			
	};

?>
