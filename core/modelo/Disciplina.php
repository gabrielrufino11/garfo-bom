<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class disciplina {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Disciplina;
		private $nome_Disciplina;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Disciplina, $nome_Disciplina) { 
			$this->id_Disciplina = $id_Disciplina;
			$this->nome_Disciplina = $nome_Disciplina;
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Disciplina;
			$this->nome_Disciplina;
		}
		
		//destructor
		function __destruct() {
			$this->id_Disciplina;
			$this->nome_Disciplina;
		}
			
	};

?>
