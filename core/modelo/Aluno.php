<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class aluno {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Aluno;
		private $nome_Aluno;
		private $id_Ano;
		private $id_Escola;
		private $id_Disciplina;
		private $senha_Aluno;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Aluno, $nome_Aluno, $id_Ano, $id_Escola, $id_Disciplina, $senha_Aluno) { 
			$this->id_Aluno = $id_Aluno;
			$this->nome_Aluno = $nome_Aluno;
			$this->id_Ano = $id_Ano;
			$this->id_Escola = $id_Escola;
			$this->id_Disciplina = $id_Disciplina;
			$this->senha_Aluno = $senha_Aluno;
						
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Aluno;
			$this->nome_Aluno;
			$this->id_Ano;
			$this->id_Escola;
			$this->id_Disciplina;
			$this->senha_Aluno;
		}
		
		//destructor
		function __destruct() {
			$this->id_Aluno;
			$this->nome_Aluno;
			$this->id_Ano;
			$this->id_Escola;
			$this->id_Disciplina;
			$this->senha_Aluno;
		}
			
	};

?>
