<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class turma {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Turma;
		private $nome_Turma;
		private $id_Ano;
		private $id_Escola;
		private $id_Disciplina;
		private $id_Aluno;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Turma, $nome_Turma, $id_Ano, $id_Escola, $id_Disciplina, $id_Aluno) { 
			$this->id_Turma = $id_Turma;
			$this->nome_Turma = $nome_Turma;
			$this->id_Ano = $id_Ano;
			$this->id_Escola = $id_Escola;
			$this->id_Disciplina = $id_Disciplina;
			$this->id_Aluno = $id_Aluno;
						
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Turma;
			$this->nome_Turma;
			$this->id_Ano;
			$this->id_Escola;
			$this->id_Disciplina;
			$this->id_Aluno;	
		}
		
		//destructor
		function __destruct() {
			$this->id_Turma;
			$this->nome_Turma;
			$this->id_Ano;
			$this->id_Escola;
			$this->id_Disciplina;
			$this->id_Aluno;
		}
			
	};

?>
