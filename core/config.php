<?php
	
	error_reporting( error_reporting() & ~E_NOTICE );

	date_default_timezone_set('America/Sao_Paulo');
	
	require_once "dados/db.php";
	require_once "dados/db.auxiliar.php";
	require_once "modelo/Turma.php";
	require_once "modelo/Aluno.php";
	require_once "modelo/Professor.php";
	require_once "modelo/Ano.php";
	require_once "modelo/Disciplina.php";
	require_once "modelo/Escola.php";
		
	
	function ExibeData($data){
		$dataCerta = explode('-', $data);
		return $dataCerta[2].'/'.$dataCerta[1].'/'.$dataCerta[0];
	}
?>