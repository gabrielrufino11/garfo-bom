<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class Turma {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Turma;
		private $nome_Turma;
		private $id_Ano;
		private $id_Escola;
		private $id_Disciplina;
	
		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Turma, $nome_Turma, $id_Ano, $id_Escola, $id_Disciplina) { 
			$this->id_Turma = $id_Turma;
			$this->nome_Turma = $nome_Turma;
			$this->id_Ano = $id_Ano;
			$this->id_Escola = $id_Escola;
			$this->id_Disciplina = $id_Disciplina;
				
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		

		public function Create() {
			
			$sql = "
				INSERT INTO turma 
						  (
				 			nome_Turma,
				 			id_Ano,
							id_Escola,
							id_Disciplina
						  )  
				VALUES 
					(
				 			
				 			'$this->nome_Turma',
				 			'$this->id_Ano',
							'$this->id_Escola',
							'$this->id_Disciplina'
					);
			";
			
			$DB = new DB();
			$DB->open();
			$result = $DB->query($sql);
			$DB->close();
			return $result;
		}
		
		//Ler Turma
		public function Read($id) {
			$sql = "
				SELECT
					 t1.id_Turma,
					 t1.nome_Turma,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.id_Disciplina
				FROM
					turma AS t1
				WHERE
					t1.id_Turma  = '$id'

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data[0]; 
		}
		
		//Ler Todas as Turmas
		public function ReadAll() {
			$sql = "
				SELECT
					 t1.id_Turma,
					 t1.nome_Turma,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.id_Disciplina
				FROM
					turma AS t1

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			$realData;
			if($Data ==NULL){
				$realData = $Data;
			}
			else{
				
				foreach($Data as $itemData){
					if(is_bool($itemData)) continue;
					else{
						$realData[] = $itemData;	
					}
				}
			}
			$DB->close();
			return $realData; 
		}
		
		//Ler Todos as Turmas com Paginação
		public function ReadAll_Paginacao($inicio, $registros) {
			$sql = "
				SELECT
					 t1.id_Turma,
					 t1.nome_Turma,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.id_Disciplina
				FROM
					turma AS t1
					
					
				LIMIT $inicio, $registros;
			";
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data; 
		}
		
		//Update Turma
		public function UpdateTurma($id, $Turma) {
			$sql = "
				UPDATE turma SET
				
				  nome_Turma = '$Turma->nome_Turma',
				  id_Ano = '$Turma->id_Ano',
				  id_Escola = '$Turma->id_Escola',
				  id_Disciplina = '$Turma->id_Disciplina'
				
				WHERE id_Turma = '$id';
				
			";
		
			$DB = new DB();
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}
		
		//Delete Turma
		public function Delete() {
			$sql = "
				DELETE FROM turma
				WHERE id_Turma = '$this->id_Turma';
			";
			$DB = new DB();
			
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
			public function ReadByOthers() {
			$sql = "
				SELECT
					 t1.id_Turma,
					 t1.nome_Turma,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.id_Disciplina
				FROM
					turma AS t1
				WHERE
					t1.nome_Turma='$this->nome_Turma' AND
					t1.id_Ano='$this->id_Ano' AND
					t1.id_Escola='$this->id_Escola' AND
					t1.id_Disciplina='$this->id_Disciplina'

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data[0]; 
		}
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Turma;
			$this->nome_Turma;
			$this->id_Ano;
			$this->id_Escola;
			$this->id_Disciplina;
				
		}
		
		//destructor
		function __destruct() {
			$this->id_Turma;
			$this->nome_Turma;
			$this->id_Ano;
			$this->id_Escola;
			$this->id_Disciplina;
			
		}
			
	};

?>
