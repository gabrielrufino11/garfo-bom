<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class Escola {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Escola;
		private $nome_Escola;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Escola,$nome_Escola) { 
			$this->id_Escola = $id_Escola;
			$this->nome_Escola = $nome_Escola;
						
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
		
		public function Create(){
			
			$sql = "
				INSERT INTO aluno 
						  (
				 			id_Escola,
				 			nome_Escola
						  )  
				VALUES 
					(
				 			'$this->id_Escola',
				 			'$this->nome_Escola'
					);
			";
			
			$DB = new DB();
			$DB->open();
			$result = $DB->query($sql);
			$DB->close();
			return $result;
		}
		
		//Ler Aluno
		public function Read($id) {
			$sql = "
				SELECT
					 t1.id_Escola,
					 t1.nome_Escola
				FROM
					escola AS t1
				WHERE
					t1.id_Escola  = '$id'

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data[0]; 
		}
		
		//Ler Todos os Alunos
		public function ReadAll() {
			$sql = "
				SELECT
					 t1.id_Escola,
					 t1.nome_Escola
				FROM
					escola AS t1
				

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			$realData;
			if($Data ==NULL){
				$realData = $Data;
			}
			else{
				
				foreach($Data as $itemData){
					if(is_bool($itemData)) continue;
					else{
						$realData[] = $itemData;	
					}
				}
			}
			$DB->close();
			return $realData; 
		}
		
		//Ler Todos os Alunos com Paginação
		public function ReadAll_Paginacao($inicio, $registros) {
			$sql = "
				SELECT
					 t1.id_Escola,
					 t1.nome_Escola
				FROM
					escola AS t1
					
					
				LIMIT $inicio, $registros;
			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data; 
		}
		
		//Update Aluno
		public function Update($id) {
			$sql = "
				UPDATE aluno SET
				  id_Escola = '$this->id_Escola',
				  nome_Escola = '$this->nome_Escola'
				 
				
				WHERE id_Escola = '$id';
				
			";
		
			
			$DB = new DB();
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}
		
		//Delete Aluno
		public function Delete($id) {
			$sql = "
				DELETE FROM aluno
				WHERE id_Escola = '$id';
			";
			$DB = new DB();
			
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}
		
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Escola;
			$this->nome_Escola;
		}
		
		//destructor
		function __destruct() {
			$this->id_Escola;
			$this->nome_Escola;
		}
			
	};

?>
