<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class Professor {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Professor;
		private $nome_Professor;
		private $email_Professor;
		private $senha_Professor;
		private $id_Escola;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Professor, $nome_Professor, $email_Professor, $senha_Professor, $id_Escola) { 
			$this->id_Professor = $id_Professor;
			$this->nome_Professor = $nome_Professor;
			$this->email_Professor = $email_Professor;
			$this->senha_Professor = $senha_Professor;
			$this->id_Escola = $id_Escola;
						
		}
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
		public function Create() {
			
			$sql = "
				INSERT INTO professor 
						  (
				 			id_Professor,
				 			nome_Professor,
				 			email_Professor,
							senha_Professor,
							id_Escola
						  )  
				VALUES 
					(
				 			'$this->id_Professor',
				 			'$this->nome_Professor',
				 			'$this->email_Professor',
							'$this->senha_Professor',
							'$this->id_Escola'
					);
			";
			
			$DB = new DB();
			$DB->open();
			$result = $DB->query($sql);
			$DB->close();
			return $result;
		}
		
		//Ler Professor
		public function Read($id) {
			$sql = "
				SELECT
					 t1.id_Professor,
					 t1.nome_Professor,
					 t1.email_Professor,
					 t1.senha_Professor,
					 t1.id_Escola
				FROM
					professor AS t1
				WHERE
					t1.id_Professor  = '$id'

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data[0]; 
		}
		
		//Ler Todos os Professores
		public function ReadAll() {
			$sql = "
				SELECT
					 t1.id_Professor,
					 t1.nome_Professor,
					 t1.email_Professor,
					 t1.senha_Professor,
					 t1.id_Escola
				FROM
					professor AS t1
				

			";
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			$realData;
			if($Data ==NULL){
				$realData = $Data;
			}
			else{
				
				foreach($Data as $itemData){
					if(is_bool($itemData)) continue;
					else{
						$realData[] = $itemData;	
					}
				}
			}
			$DB->close();
			return $realData; 
		}
		
		//Ler Todos os Professores com Paginação
		public function LerTodosProfessores_Paginacao($inicio, $registros) {
			$sql = "
				SELECT
					 t1.id_Professor,
					 t1.nome_Professor,
					 t1.email_Professor,
					 t1.senha_Professor,
					 t1.id_Escola
				FROM
					professor AS t1
					
					
				LIMIT $inicio, $registros;
			";
			
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data; 
		}
		
		//Update Professor
		public function Update($id) {
			$sql = "
				UPDATE professor SET
				
				  nome_Professor = '$this->nome_Professor',
				  email_Professor = '$this->email_Professor',
				  senha_Professor = '$this->senha_Professor',
				  id_Escola = '$this->id_Escola'
				
				WHERE id_Professor = '$id';
				
			";
		
			
			$DB = new DB();
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}
		
		//Delete Professor
		public function DeleteProfessor($id) {
			$sql = "
				DELETE FROM professor
				WHERE id_Professor = '$id';
			";
			$DB = new DB();
			
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		public function ReadByEmail($email){
			$sql = "
				SELECT
					 t1.id_Professor,
					 t1.nome_Professor,
					 t1.email_Professor,
					 t1.senha_Professor,
					 t1.id_Escola

				FROM
					professor AS t1
				WHERE
					t1.email_Professor = '$email'

			";


			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);

			$DB->close();
			return $Data[0];
		}
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Professor;
			$this->nome_Professor;
			$this->email_Professor;
			$this->senha_Professor;
			$this->id_Escola;
		}
		
		//destructor
		function __destruct() {
			$this->id_Professor;
			$this->nome_Professor;
			$this->email_Professor;
			$this->senha_Professor;
			$this->id_Escola;
		}
			
	};

?>
