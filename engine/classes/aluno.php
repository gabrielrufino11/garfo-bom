<?php
	//Declaracao da classe
	//Nome da classe devera ser o nome da tabela respectiva no banco de dados
	class Aluno {
		
		//Variaveis da classe
		//Nome das variaveis devem ser de acordo com as colunas da tabela respectiva no bd
		private $id_Aluno;
		private $nome_Aluno;
		private $id_Ano;
		private $id_Escola;
		private $senha_Aluno;
		private $id_Turma;

		//setters
		
		//Funcao que seta uma instancia da classe
		public function SetValues($id_Aluno, $nome_Aluno, $id_Ano, $id_Escola, $senha_Aluno,$id_Turma) { 
			$this->id_Aluno = $id_Aluno;
			$this->nome_Aluno = $nome_Aluno;
			$this->id_Ano = $id_Ano;
			$this->id_Escola = $id_Escola;
			$this->senha_Aluno = $senha_Aluno;
			$this->id_Turma = $id_Turma;
		}
		
		public function __get($property) {
    		if (property_exists($this, $property)) {
      			return $this->$property;
    		}
  		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
			return $this;
		}
		
				
		public function Create(){
			
			$sql = "
				INSERT INTO aluno 
						  (
				 			id_Aluno,
				 			nome_Aluno,
				 			id_Ano,
							id_Escola,
							senha_Aluno,
							id_Turma
						  )  
				VALUES 
					(
				 			'$this->id_Aluno',
				 			'$this->nome_Aluno',
				 			'$this->id_Ano',
							'$this->id_Escola',
							'$this->senha_Aluno',
							'$this->id_Turma'
					);
			";
			
			$DB = new DB();
			$DB->open();
			$result = $DB->query($sql);
			$DB->close();
			return $result;
		}
		
		//Ler Aluno
		public function Read($id) {
			$sql = "
				SELECT
					 t1.id_Aluno,
					 t1.nome_Aluno,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.senha_Aluno,
					 t1.id_Turma
				FROM
					aluno AS t1
				WHERE
					t1.id_Aluno  = '$id'

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data[0]; 
		}
		
		//Ler Todos os Alunos
		public function ReadAll() {
			$sql = "
				SELECT
					 t1.id_Aluno,
					 t1.nome_Aluno,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.senha_Aluno,
					 t1.id_Turma
				FROM
					aluno AS t1
				

			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			$realData;
			if($Data ==NULL){
				$realData = $Data;
			}
			else{
				
				foreach($Data as $itemData){
					if(is_bool($itemData)) continue;
					else{
						$realData[] = $itemData;	
					}
				}
			}
			$DB->close();
			return $realData; 
		}
		
		//Ler Todos os Alunos com Paginação
		public function ReadAll_Paginacao($inicio, $registros) {
			$sql = "
				SELECT
					 t1.id_Aluno,
					 t1.nome_Aluno,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.senha_Aluno,
					 t1.id_Turma
				FROM
					aluno AS t1
					
					
				LIMIT $inicio, $registros;
			";
			
			
			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);
			
			$DB->close();
			return $Data; 
		}
		
		//Update Aluno
		public function Update($id) {
			$sql = "
				UPDATE aluno SET
				
				  nome_Aluno = '$this->nome_Aluno',
				  id_Ano = '$this->id_Ano',
				  id_Escola = '$this->id_Escola',
				  senha_Aluno = '$this->senha_Aluno',
				  id_Turma = '$this->id_Turma'
				
				WHERE id_Aluno = '$id';
				
			";
		
			
			$DB = new DB();
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}

		public function UpdateTurma($id) {
		
			$sql = "
				UPDATE aluno SET
				
				 id_Ano = '$this->id_Ano',
				  id_Escola = '$this->id_Escola',
				  id_Turma = '$this->id_Turma'
				
				WHERE id_Aluno = '$id';
				
			";
		
			
			$DB = new DB();
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			
			return $result;
		}
		
		//Delete Aluno
		public function Delete($id) {
			$sql = "
				DELETE FROM aluno
				WHERE id_Aluno = '$id';
			";
			$DB = new DB();
			
			$DB->open();
			$result =$DB->query($sql);
			$DB->close();
			return $result;
		}
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- begin 
			--------------------------------------------------
		
		*/
		
		public function ReadByEmail($email){
			$sql = "
				SELECT
					 t1.id_Aluno,
					 t1.nome_Aluno,
					 t1.id_Ano,
					 t1.id_Escola,
					 t1.senha_Aluno,
					 t1.id_Turma

				FROM
					aluno AS t1
				WHERE
					t1.nome_Aluno = '$email'

			";


			$DB = new DB();
			$DB->open();
			$Data = $DB->fetchData($sql);

			$DB->close();
			return $Data[0];
		}
		
		
		/*
			--------------------------------------------------
			Viewer SPecific methods -- end 
			--------------------------------------------------
		
		*/
		
		
		//constructor 
		
		function __construct() { 
			$this->id_Aluno;
			$this->nome_Aluno;
			$this->id_Ano;
			$this->id_Escola;
			$this->senha_Aluno;
			$this->id_Turma;
		}
		
		//destructor
		function __destruct() {
			$this->id_Aluno;
			$this->nome_Aluno;
			$this->id_Ano;
			$this->id_Escola;
			$this->senha_Aluno;
			$this->id_Turma;
		}
			
	};

?>
