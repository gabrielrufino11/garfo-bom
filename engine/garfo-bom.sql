SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `aluno`
-- ----------------------------
DROP TABLE IF EXISTS `aluno`;
CREATE TABLE `aluno` (
  `id_Aluno` int(11) NOT NULL AUTO_INCREMENT,
  `nome_Aluno` varchar(100) NOT NULL,
  `id_Ano` int(11) NOT NULL,
  `id_Escola` int(11) NOT NULL,
  `id_Disciplina` int(11) NOT NULL,
  `senha_Aluno` varchar(100) NOT NULL,
  `id_Turma` int(11) NOT NULL,
  PRIMARY KEY (`id_Aluno`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aluno
-- ----------------------------
INSERT INTO `aluno` VALUES ('1', 'Natan', '1', '1', '1', '', '1');
INSERT INTO `aluno` VALUES ('2', 'Gabriel', '3', '1', '2', '', '46');
INSERT INTO `aluno` VALUES ('3', 'Luiz', '1', '1', '1', '', '1');
INSERT INTO `aluno` VALUES ('4', 'Magno', '3', '1', '2', '', '46');
INSERT INTO `aluno` VALUES ('5', 'Mari', '3', '1', '1', '', '66');
INSERT INTO `aluno` VALUES ('6', 'natantur7@gmail.com', '1', '1', '2', 'HPPHZR31411618', '0');

-- ----------------------------
-- Table structure for `ano`
-- ----------------------------
DROP TABLE IF EXISTS `ano`;
CREATE TABLE `ano` (
  `id_Ano` int(11) NOT NULL AUTO_INCREMENT,
  `nome_Ano` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Ano`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ano
-- ----------------------------
INSERT INTO `ano` VALUES ('1', '1º Ano');
INSERT INTO `ano` VALUES ('2', '2º Ano');
INSERT INTO `ano` VALUES ('3', '3º Ano');
INSERT INTO `ano` VALUES ('4', '4º Ano');
INSERT INTO `ano` VALUES ('5', '5º Ano');

-- ----------------------------
-- Table structure for `disciplina`
-- ----------------------------
DROP TABLE IF EXISTS `disciplina`;
CREATE TABLE `disciplina` (
  `id_Disciplina` int(11) NOT NULL AUTO_INCREMENT,
  `nome_Disciplina` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Disciplina`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of disciplina
-- ----------------------------
INSERT INTO `disciplina` VALUES ('1', 'Matemática');
INSERT INTO `disciplina` VALUES ('2', 'Português');
INSERT INTO `disciplina` VALUES ('3', 'Geografia');

-- ----------------------------
-- Table structure for `escola`
-- ----------------------------
DROP TABLE IF EXISTS `escola`;
CREATE TABLE `escola` (
  `id_Escola` int(11) NOT NULL AUTO_INCREMENT,
  `nome_Escola` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Escola`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of escola
-- ----------------------------
INSERT INTO `escola` VALUES ('1', 'Belita');

-- ----------------------------
-- Table structure for `professor`
-- ----------------------------
DROP TABLE IF EXISTS `professor`;
CREATE TABLE `professor` (
  `id_Professor` int(11) NOT NULL AUTO_INCREMENT,
  `nome_Professor` varchar(100) NOT NULL,
  `email_Professor` varchar(100) NOT NULL,
  `senha_Professor` varchar(40) NOT NULL,
  `id_Escola` int(11) NOT NULL,
  PRIMARY KEY (`id_Professor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of professor
-- ----------------------------

-- ----------------------------
-- Table structure for `turma`
-- ----------------------------
DROP TABLE IF EXISTS `turma`;
CREATE TABLE `turma` (
  `id_Turma` int(11) NOT NULL AUTO_INCREMENT,
  `nome_Turma` varchar(100) NOT NULL,
  `id_Ano` int(11) NOT NULL,
  `id_Escola` int(11) NOT NULL,
  `id_Disciplina` int(11) NOT NULL,
  PRIMARY KEY (`id_Turma`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of turma
-- ----------------------------
INSERT INTO `turma` VALUES ('1', 'teste', '1', '1', '1');
