<?php
	
	error_reporting(E_ALL && ~E_NOTICE);

	date_default_timezone_set('America/Sao_Paulo');
	
	require_once "bd/bd.php";
	require_once "classes/turma.php";
	require_once "classes/aluno.php";
	require_once "classes/professor.php";
	require_once "classes/ano.php";
	require_once "classes/disciplina.php";
	require_once "classes/escola.php";
		
	
	function ExibeData($data){
		$dataCerta = explode('-', $data);
		return $dataCerta[2].'/'.$dataCerta[1].'/'.$dataCerta[0];
	}
?>